/**
 * Huffman encoder Header file for the huffman encoder program.
 * */ 

#ifndef _huffman
#define _huffman

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <memory>           //for shared pointer
#include <queue>            //for priority queue


//Classes    
class HuffmanNode{

public:
    char letter;                                     //Node's specific character
    int frequency;                              //Frequency of the character
    bool isLeaf;
    std::shared_ptr<HuffmanNode> left, right;   //POinters to the left and right children of the node    

    // Constructor
    HuffmanNode() : letter('\0'), frequency(0), isLeaf(false), left(nullptr), right(nullptr) {}
    HuffmanNode(char c, int f) : letter(c), frequency(f), isLeaf(true), left(nullptr), right(nullptr) {}
    HuffmanNode(int f, std::shared_ptr<HuffmanNode> l, std::shared_ptr<HuffmanNode> r) : frequency(f), isLeaf(false), left(l), right(r) {}

    //Destructor
    ~HuffmanNode(){
        if(letter != '\0')
        {
            frequency=0;
            letter='\0';
            left = nullptr;     //redundant. Shared pointer will be taken care of as the destruct is called
            right = nullptr;
        }
    }

    //Copy Constructor
    HuffmanNode(const HuffmanNode & rhs) : letter(rhs.letter), frequency(rhs.frequency), isLeaf(rhs.isLeaf){
        left = rhs.left;
        right = rhs.right;
    }

    //Copy Assignment
    HuffmanNode & operator=(const HuffmanNode & rhs){
        if(this != &rhs){
            letter = rhs.letter;
            frequency = rhs.frequency;
            isLeaf = rhs.isLeaf;
            left = rhs.left;
            right = rhs.right;
        }
        return *this;
    }

    //Move Constructor
    HuffmanNode( HuffmanNode && rhs) : letter(std::move(rhs.letter)), frequency(std::move(rhs.frequency)), isLeaf(rhs.isLeaf), left(std::move(rhs.left)), right(std::move(rhs.right))
    {
        rhs.frequency = 0;
        rhs.letter = '\0';
    }

    //Move Assignment
    HuffmanNode & operator=( HuffmanNode && rhs){
        if(this != &rhs){
            letter = std::move(rhs.letter);     //move operator
            frequency = rhs.frequency;          //copy this from rhs
            isLeaf = rhs.isLeaf;
            left = nullptr;
            right=nullptr;                      //release this resource
            left = std::move(rhs.left);
            right = std::move(rhs.right);       
            rhs.letter = '\0';                  //release rhs's resource
        }
        return *this;
    }

    void printNode()
    {
        std::cout << "I am node: "<< letter << " ("<<frequency<<") Leaf? "<< isLeaf << std::endl;
    }

};//==== end Huffman Node


//Overloading the < operator for the Huffman Node
bool operator < (const HuffmanNode &a, const HuffmanNode &b)
{
    /**
     * Return true when a has a higher frequency than b
     * */
    return a.frequency > b.frequency;
}
bool operator < (const std::shared_ptr<HuffmanNode> &a, const std::shared_ptr<HuffmanNode> &b)
{
    /**
     * Return true when a has a higher frequency than b
     * */
    return a->frequency > b->frequency;
}

/* ------------------------------------------------------------------------
*  ----------------------- HUFFMAN TREE -----------------------------------
*  -----------------------------------------------------------------------*/
class HuffmanTree{

    public:
        std::shared_ptr<HuffmanNode> root;
        std::priority_queue< std::shared_ptr<HuffmanNode> > myQ;  //Priority queue in min heap form so min freq is at the top
        std::unordered_map<char, std::string> codeTable;               //Relates each letter to the code used to get to the letter in the binary tree

        //Constructor
        HuffmanTree() : root(nullptr){}

        //Destructor
        ~HuffmanTree(){}

        //Generate Tree
        void generateTree(std::unordered_map<char, int> &u)
        {
            /** For each pair in the unordered map, create a shared pointer to a HuffmanNode in heap.
             *  Ordered so that the min freq node is at the top.
             *  Results in a single tree forest.
             */
            for (const auto &n : u)
            {
                std::shared_ptr<HuffmanNode> snode( new HuffmanNode(n.first , n.second ) );
                myQ.push(std::move(snode));
            }

            /** Pop smallest two nodes from the tree.
             *  Create an inner node as parent node for these smallest two nodes.
             *  Set the parent freq to sum of children freq.
             *  Reinsert into the queue
             *  Continue until queue has only one node.
             * */
            while(myQ.size() > 1)
            {
                //Pop two smallest nodes
                std::shared_ptr<HuffmanNode> n1 = std::move(myQ.top());
                myQ.pop();
                std::shared_ptr<HuffmanNode> n2 = std::move(myQ.top());
                myQ.pop();
                if( n1->frequency < n2->frequency ){ //Place n1 as left child
                    std::shared_ptr<HuffmanNode> inner( new HuffmanNode( (n1->frequency+n2->frequency), std::move(n1), std::move(n2) ) );
                    myQ.push(std::move(inner));
                }else{                               //Place n2 as left child
                    std::shared_ptr<HuffmanNode> inner( new HuffmanNode( (n1->frequency+n2->frequency), std::move(n2), std::move(n1) ) );
                    myQ.push(std::move(inner));
                }
            }    
            root = std::move(myQ.top());
        }

        //In Order Traversal
        void inOrder(std::shared_ptr<HuffmanNode> node, std::string code)
        {
            //string s;
            if(node == nullptr){
                //base case. Stop
                return;
            }else{
                inOrder(node->left, code+"0");            //Recurse left
                if(node->isLeaf){                    
                    // node->printNode();
                    codeTable[node->letter] = code;       //Add the code to the table
                    return;
                }
                inOrder(node->right, code+"1");           //Recurse right
            }
        }

        //Print the code table
        void printCodeTable()
        {
            for (const auto &n : codeTable)
                std::cout << n.first << " : " << n.second << std::endl;
        }

};//==== end Huffman Tree

//----- Helper Fucntions
void printHeader(const std::string op_file_name, const HuffmanTree &ht, const std::string byteStream, const std::string outputBitStream)
{
    //Write the bitstream output to the output file
    try
    {
        std::ofstream outputhdr("../out/"+op_file_name+".hdr");
        //Get the number of characters in the file.
        int fieldCount = ht.root->frequency;
        outputhdr << "Number of characters : " << fieldCount << std::endl;
        outputhdr << "Number of Bytes to represent input : " << fieldCount*8 << " (uncompressed)" <<  std::endl;
        outputhdr << "Number of Bytes to represent input : " << byteStream.size()/8 << " (compressed)" <<  std::endl << std::endl;

        outputhdr << "---- Code Table ----" << std::endl;
        for(const auto &n : ht.codeTable)
            outputhdr << n.first << " : " << n.second << std::endl;
        outputhdr << "--------------------" << std::endl << std::endl;

        outputhdr << "---- Huffman Encoded Bit Stream ----" << std::endl;
        outputhdr << outputBitStream;
        outputhdr.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
}

// void printBinFile(const string op_file_name, const char *byteArray)
// {
//         //Pack them bytes
//     try
//     {
//         ofstream binOut("../out/"+op_file_name+".bin");
//         for(char z: byteArray)
//             binOut << z;
//         binOut.close();
//     }
//     catch(const std::exception& e)
//     {
//         std::cerr << e.what() << '\n';
//     }
// }

#endif

