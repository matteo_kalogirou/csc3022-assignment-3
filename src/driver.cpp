#include "huffencode.h"

using namespace std;

int main(int argc, char *argv[])
{
    char c;
    vector<char> inputCharVec;
    unordered_map<char, int> u;     //Stores the frequency of each ASCII char found in the text file
    string op_file_name;

    // Check the input arguments to make sure that enough parameters are entered
    if(argc <3 ) { throw "Not enough input arguments."; }
    else
    {
        op_file_name = argv[2];
        ifstream input_file (argv[1]);
        if(!input_file.is_open()){
            cout << "Cannot find the file " << argv[1] << endl;
            throw "No such file found.";
        }else{
            while( input_file.get(c) )
            {
                //If the value pair hasn't been added
                if(u[c] < 0)
                {
                    u[c] = 1;
                }
                else //update the value pair
                {
                    u[c] = u[c]+1;
                }
                //add the char to the input char array
                inputCharVec.push_back(c);
            }   
        }
        input_file.close();
    }


//Create a Huffman Tree
HuffmanTree *ht = new HuffmanTree();
ht->generateTree(u);
//Genrate the Code Table
ht->inOrder(ht->root, "");


//Generate the output bitstream
string outputBitStream = "";
for(char c: inputCharVec)
    {
        outputBitStream = outputBitStream + ht->codeTable[c];
    }
//Buffer the bit stream so that it has size of mutiple 8.
string byteStream = outputBitStream;
for(int i =0; i < 8-outputBitStream.size()%8; i++)
    byteStream = byteStream + "0";


//Get number of bytes to represent encoded data
char byteArray [byteStream.size()/8];
//Represent encoded data with bytes
for(int i=0; i<byteStream.size(); i+=8)
{
    string byte = "";
    for(int j=0; j<8; j++)
        byte = byte + byteStream[i+j];
    byteArray[i/8] = stol(byte, nullptr, 2);
}

//Pack them bytes
try
{
    ofstream binOut("../out/"+op_file_name+".bin");
    for(char z: byteArray)
        binOut << z;
    binOut.close();
}
catch(const std::exception& e)
{
    std::cerr << e.what() << '\n';
}

printHeader(op_file_name, *ht, byteStream, outputBitStream);


delete ht;
    return 0;
}


